import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DinnerPage } from '../dinner/dinner';
import { HealthyMenusPage } from '../healthy-menus/healthy-menus';
import { BreakfastPage } from '../breakfast/breakfast';
import { BanofeeStuffedPanCakePage } from '../banofee-stuffed-pan-cake/banofee-stuffed-pan-cake';
import { FruityOatCakePage } from '../fruity-oat-cake/fruity-oat-cake';
import { LunchPage } from '../lunch/lunch';
import { ChickpeaPitaPage } from '../chickpea-pita/chickpea-pita';
import { BlackBeanBurritoPage } from '../black-bean-burrito/black-bean-burrito';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';
import { SpinachPiciPastaPage } from '../spinach-pici-pasta/spinach-pici-pasta';
import { ChineseSteakStewPage } from '../chinese-steak-stew/chinese-steak-stew';

@Component({
  selector: 'page-chinese-steak-stew',
  templateUrl: 'chinese-steak-stew.html'
})
export class ChineseSteakStewPage {

  constructor(public navCtrl: NavController) {
  }
  goToDinner(params){
    if (!params) params = {};
    this.navCtrl.push(DinnerPage);
  }goToHealthyMenus(params){
    if (!params) params = {};
    this.navCtrl.push(HealthyMenusPage);
  }goToBreakfast(params){
    if (!params) params = {};
    this.navCtrl.push(BreakfastPage);
  }goToBanofeeStuffedPanCake(params){
    if (!params) params = {};
    this.navCtrl.push(BanofeeStuffedPanCakePage);
  }goToFruityOatCake(params){
    if (!params) params = {};
    this.navCtrl.push(FruityOatCakePage);
  }goToLunch(params){
    if (!params) params = {};
    this.navCtrl.push(LunchPage);
  }goToChickpeaPita(params){
    if (!params) params = {};
    this.navCtrl.push(ChickpeaPitaPage);
  }goToBlackBeanBurrito(params){
    if (!params) params = {};
    this.navCtrl.push(BlackBeanBurritoPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }goToSpinachPiciPasta(params){
    if (!params) params = {};
    this.navCtrl.push(SpinachPiciPastaPage);
  }goToChineseSteakStew(params){
    if (!params) params = {};
    this.navCtrl.push(ChineseSteakStewPage);
  }
}
