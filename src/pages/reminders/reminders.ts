import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MedicationsPage } from '../medications/medications';
import { RemindersPage } from '../reminders/reminders';
import { DoctorsAppointmentsPage } from '../doctors-appointments/doctors-appointments';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-reminders',
  templateUrl: 'reminders.html'
})
export class RemindersPage {

  constructor(public navCtrl: NavController) {
  }
  goToMedications(params){
    if (!params) params = {};
    this.navCtrl.push(MedicationsPage);
  }goToReminders(params){
    if (!params) params = {};
    this.navCtrl.push(RemindersPage);
  }goToDoctorsAppointments(params){
    if (!params) params = {};
    this.navCtrl.push(DoctorsAppointmentsPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }
}
