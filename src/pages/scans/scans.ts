import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MostRecentHeadMRIPage } from '../most-recent-head-mri/most-recent-head-mri';
import { ScansPage } from '../scans/scans';
import { MostRecentHandMRIPage } from '../most-recent-hand-mri/most-recent-hand-mri';
import { MostRecentLungsXrayPage } from '../most-recent-lungs-xray/most-recent-lungs-xray';
import { HistoryPage } from '../history/history';
import { AllergiesPage } from '../allergies/allergies';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-scans',
  templateUrl: 'scans.html'
})
export class ScansPage {

  constructor(public navCtrl: NavController) {
  }
  goToMostRecentHeadMRI(params){
    if (!params) params = {};
    this.navCtrl.push(MostRecentHeadMRIPage);
  }goToScans(params){
    if (!params) params = {};
    this.navCtrl.push(ScansPage);
  }goToMostRecentHandMRI(params){
    if (!params) params = {};
    this.navCtrl.push(MostRecentHandMRIPage);
  }goToMostRecentLungsXray(params){
    if (!params) params = {};
    this.navCtrl.push(MostRecentLungsXrayPage);
  }goToHistory(params){
    if (!params) params = {};
    this.navCtrl.push(HistoryPage);
  }goToAllergies(params){
    if (!params) params = {};
    this.navCtrl.push(AllergiesPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }
}
