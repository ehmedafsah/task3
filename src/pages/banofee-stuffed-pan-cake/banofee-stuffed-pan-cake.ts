import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BreakfastPage } from '../breakfast/breakfast';
import { BanofeeStuffedPanCakePage } from '../banofee-stuffed-pan-cake/banofee-stuffed-pan-cake';
import { FruityOatCakePage } from '../fruity-oat-cake/fruity-oat-cake';
import { HealthyMenusPage } from '../healthy-menus/healthy-menus';
import { LunchPage } from '../lunch/lunch';
import { ChickpeaPitaPage } from '../chickpea-pita/chickpea-pita';
import { BlackBeanBurritoPage } from '../black-bean-burrito/black-bean-burrito';
import { DinnerPage } from '../dinner/dinner';
import { SpinachPiciPastaPage } from '../spinach-pici-pasta/spinach-pici-pasta';
import { ChineseSteakStewPage } from '../chinese-steak-stew/chinese-steak-stew';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-banofee-stuffed-pan-cake',
  templateUrl: 'banofee-stuffed-pan-cake.html'
})
export class BanofeeStuffedPanCakePage {

  constructor(public navCtrl: NavController) {
  }
  goToBreakfast(params){
    if (!params) params = {};
    this.navCtrl.push(BreakfastPage);
  }goToBanofeeStuffedPanCake(params){
    if (!params) params = {};
    this.navCtrl.push(BanofeeStuffedPanCakePage);
  }goToFruityOatCake(params){
    if (!params) params = {};
    this.navCtrl.push(FruityOatCakePage);
  }goToHealthyMenus(params){
    if (!params) params = {};
    this.navCtrl.push(HealthyMenusPage);
  }goToLunch(params){
    if (!params) params = {};
    this.navCtrl.push(LunchPage);
  }goToChickpeaPita(params){
    if (!params) params = {};
    this.navCtrl.push(ChickpeaPitaPage);
  }goToBlackBeanBurrito(params){
    if (!params) params = {};
    this.navCtrl.push(BlackBeanBurritoPage);
  }goToDinner(params){
    if (!params) params = {};
    this.navCtrl.push(DinnerPage);
  }goToSpinachPiciPasta(params){
    if (!params) params = {};
    this.navCtrl.push(SpinachPiciPastaPage);
  }goToChineseSteakStew(params){
    if (!params) params = {};
    this.navCtrl.push(ChineseSteakStewPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }
}
