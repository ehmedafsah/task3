import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ScansPage } from '../scans/scans';
import { MostRecentHeadMRIPage } from '../most-recent-head-mri/most-recent-head-mri';
import { MostRecentHandMRIPage } from '../most-recent-hand-mri/most-recent-hand-mri';
import { MostRecentLungsXrayPage } from '../most-recent-lungs-xray/most-recent-lungs-xray';
import { HistoryPage } from '../history/history';
import { AllergiesPage } from '../allergies/allergies';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-most-recent-lungs-xray',
  templateUrl: 'most-recent-lungs-xray.html'
})
export class MostRecentLungsXrayPage {

  constructor(public navCtrl: NavController) {
  }
  goToScans(params){
    if (!params) params = {};
    this.navCtrl.push(ScansPage);
  }goToMostRecentHeadMRI(params){
    if (!params) params = {};
    this.navCtrl.push(MostRecentHeadMRIPage);
  }goToMostRecentHandMRI(params){
    if (!params) params = {};
    this.navCtrl.push(MostRecentHandMRIPage);
  }goToMostRecentLungsXray(params){
    if (!params) params = {};
    this.navCtrl.push(MostRecentLungsXrayPage);
  }goToHistory(params){
    if (!params) params = {};
    this.navCtrl.push(HistoryPage);
  }goToAllergies(params){
    if (!params) params = {};
    this.navCtrl.push(AllergiesPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }
}
