import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Page27Page } from '../page27/page27';
import { NewsPage } from '../news/news';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html'
})
export class NewsPage {

  constructor(public navCtrl: NavController) {
  }
  goToPage27(params){
    if (!params) params = {};
    this.navCtrl.push(Page27Page);
  }goToNews(params){
    if (!params) params = {};
    this.navCtrl.push(NewsPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }
}
