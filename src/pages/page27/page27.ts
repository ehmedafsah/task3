import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewsPage } from '../news/news';
import { Page27Page } from '../page27/page27';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-page27',
  templateUrl: 'page27.html'
})
export class Page27Page {

  constructor(public navCtrl: NavController) {
  }
  goToNews(params){
    if (!params) params = {};
    this.navCtrl.push(NewsPage);
  }goToPage27(params){
    if (!params) params = {};
    this.navCtrl.push(Page27Page);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }
}
