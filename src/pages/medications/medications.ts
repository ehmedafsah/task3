import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RemindersPage } from '../reminders/reminders';
import { MedicationsPage } from '../medications/medications';
import { DoctorsAppointmentsPage } from '../doctors-appointments/doctors-appointments';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-medications',
  templateUrl: 'medications.html'
})
export class MedicationsPage {

  constructor(public navCtrl: NavController) {
  }
  goToReminders(params){
    if (!params) params = {};
    this.navCtrl.push(RemindersPage);
  }goToMedications(params){
    if (!params) params = {};
    this.navCtrl.push(MedicationsPage);
  }goToDoctorsAppointments(params){
    if (!params) params = {};
    this.navCtrl.push(DoctorsAppointmentsPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }
}
