import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LunchPage } from '../lunch/lunch';
import { HealthyMenusPage } from '../healthy-menus/healthy-menus';
import { BreakfastPage } from '../breakfast/breakfast';
import { BanofeeStuffedPanCakePage } from '../banofee-stuffed-pan-cake/banofee-stuffed-pan-cake';
import { FruityOatCakePage } from '../fruity-oat-cake/fruity-oat-cake';
import { DinnerPage } from '../dinner/dinner';
import { SpinachPiciPastaPage } from '../spinach-pici-pasta/spinach-pici-pasta';
import { ChineseSteakStewPage } from '../chinese-steak-stew/chinese-steak-stew';
import { MainMenuPage } from '../main-menu/main-menu';
import { ProfilePage } from '../profile/profile';
import { ChickpeaPitaPage } from '../chickpea-pita/chickpea-pita';
import { BlackBeanBurritoPage } from '../black-bean-burrito/black-bean-burrito';

@Component({
  selector: 'page-black-bean-burrito',
  templateUrl: 'black-bean-burrito.html'
})
export class BlackBeanBurritoPage {

  constructor(public navCtrl: NavController) {
  }
  goToLunch(params){
    if (!params) params = {};
    this.navCtrl.push(LunchPage);
  }goToHealthyMenus(params){
    if (!params) params = {};
    this.navCtrl.push(HealthyMenusPage);
  }goToBreakfast(params){
    if (!params) params = {};
    this.navCtrl.push(BreakfastPage);
  }goToBanofeeStuffedPanCake(params){
    if (!params) params = {};
    this.navCtrl.push(BanofeeStuffedPanCakePage);
  }goToFruityOatCake(params){
    if (!params) params = {};
    this.navCtrl.push(FruityOatCakePage);
  }goToDinner(params){
    if (!params) params = {};
    this.navCtrl.push(DinnerPage);
  }goToSpinachPiciPasta(params){
    if (!params) params = {};
    this.navCtrl.push(SpinachPiciPastaPage);
  }goToChineseSteakStew(params){
    if (!params) params = {};
    this.navCtrl.push(ChineseSteakStewPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.push(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.push(ProfilePage);
  }goToChickpeaPita(params){
    if (!params) params = {};
    this.navCtrl.push(ChickpeaPitaPage);
  }goToBlackBeanBurrito(params){
    if (!params) params = {};
    this.navCtrl.push(BlackBeanBurritoPage);
  }
}
