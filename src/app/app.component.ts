import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { RemindersPage } from '../pages/reminders/reminders';
import { MedicationsPage } from '../pages/medications/medications';
import { DoctorsAppointmentsPage } from '../pages/doctors-appointments/doctors-appointments';
import { MainMenuPage } from '../pages/main-menu/main-menu';
import { ProfilePage } from '../pages/profile/profile';
import { CameraTabDefaultPagePage } from '../pages/camera-tab-default-page/camera-tab-default-page';
import { HistoryPage } from '../pages/history/history';
import { AllergiesPage } from '../pages/allergies/allergies';
import { ScansPage } from '../pages/scans/scans';
import { MostRecentHeadMRIPage } from '../pages/most-recent-head-mri/most-recent-head-mri';
import { MostRecentHandMRIPage } from '../pages/most-recent-hand-mri/most-recent-hand-mri';
import { MostRecentLungsXrayPage } from '../pages/most-recent-lungs-xray/most-recent-lungs-xray';
import { HealthyMenusPage } from '../pages/healthy-menus/healthy-menus';
import { BreakfastPage } from '../pages/breakfast/breakfast';
import { BanofeeStuffedPanCakePage } from '../pages/banofee-stuffed-pan-cake/banofee-stuffed-pan-cake';
import { FruityOatCakePage } from '../pages/fruity-oat-cake/fruity-oat-cake';
import { LunchPage } from '../pages/lunch/lunch';
import { ChickpeaPitaPage } from '../pages/chickpea-pita/chickpea-pita';
import { BlackBeanBurritoPage } from '../pages/black-bean-burrito/black-bean-burrito';
import { DinnerPage } from '../pages/dinner/dinner';
import { SpinachPiciPastaPage } from '../pages/spinach-pici-pasta/spinach-pici-pasta';
import { ChineseSteakStewPage } from '../pages/chinese-steak-stew/chinese-steak-stew';
import { NewsPage } from '../pages/news/news';
import { Page27Page } from '../pages/page27/page27';


import { WelcomePage } from '../pages/welcome/welcome';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
    rootPage:any = WelcomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  goToReminders(params){
    if (!params) params = {};
    this.navCtrl.setRoot(RemindersPage);
  }goToMedications(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MedicationsPage);
  }goToDoctorsAppointments(params){
    if (!params) params = {};
    this.navCtrl.setRoot(DoctorsAppointmentsPage);
  }goToMainMenu(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MainMenuPage);
  }goToProfile(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ProfilePage);
  }goToWelcome(params){
    if (!params) params = {};
    this.navCtrl.setRoot(WelcomePage);
  }goToCameraTabDefaultPage(params){
    if (!params) params = {};
    this.navCtrl.setRoot(CameraTabDefaultPagePage);
  }goToHistory(params){
    if (!params) params = {};
    this.navCtrl.setRoot(HistoryPage);
  }goToAllergies(params){
    if (!params) params = {};
    this.navCtrl.setRoot(AllergiesPage);
  }goToScans(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ScansPage);
  }goToMostRecentHeadMRI(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MostRecentHeadMRIPage);
  }goToMostRecentHandMRI(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MostRecentHandMRIPage);
  }goToMostRecentLungsXray(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MostRecentLungsXrayPage);
  }goToHealthyMenus(params){
    if (!params) params = {};
    this.navCtrl.setRoot(HealthyMenusPage);
  }goToBreakfast(params){
    if (!params) params = {};
    this.navCtrl.setRoot(BreakfastPage);
  }goToBanofeeStuffedPanCake(params){
    if (!params) params = {};
    this.navCtrl.setRoot(BanofeeStuffedPanCakePage);
  }goToFruityOatCake(params){
    if (!params) params = {};
    this.navCtrl.setRoot(FruityOatCakePage);
  }goToLunch(params){
    if (!params) params = {};
    this.navCtrl.setRoot(LunchPage);
  }goToChickpeaPita(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ChickpeaPitaPage);
  }goToBlackBeanBurrito(params){
    if (!params) params = {};
    this.navCtrl.setRoot(BlackBeanBurritoPage);
  }goToDinner(params){
    if (!params) params = {};
    this.navCtrl.setRoot(DinnerPage);
  }goToSpinachPiciPasta(params){
    if (!params) params = {};
    this.navCtrl.setRoot(SpinachPiciPastaPage);
  }goToChineseSteakStew(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ChineseSteakStewPage);
  }goToNews(params){
    if (!params) params = {};
    this.navCtrl.setRoot(NewsPage);
  }goToPage27(params){
    if (!params) params = {};
    this.navCtrl.setRoot(Page27Page);
  }
}
