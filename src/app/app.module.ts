import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { CameraTabDefaultPagePage } from '../pages/camera-tab-default-page/camera-tab-default-page';
import { CartTabDefaultPagePage } from '../pages/cart-tab-default-page/cart-tab-default-page';
import { CloudTabDefaultPagePage } from '../pages/cloud-tab-default-page/cloud-tab-default-page';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { WelcomePage } from '../pages/welcome/welcome';
import { RemindersPage } from '../pages/reminders/reminders';
import { MedicationsPage } from '../pages/medications/medications';
import { DoctorsAppointmentsPage } from '../pages/doctors-appointments/doctors-appointments';
import { HealthyMenusPage } from '../pages/healthy-menus/healthy-menus';
import { BreakfastPage } from '../pages/breakfast/breakfast';
import { Tab4DefaultPagePage } from '../pages/tab4default-page/tab4default-page';
import { BanofeeStuffedPanCakePage } from '../pages/banofee-stuffed-pan-cake/banofee-stuffed-pan-cake';
import { FruityOatCakePage } from '../pages/fruity-oat-cake/fruity-oat-cake';
import { LunchPage } from '../pages/lunch/lunch';
import { ChickpeaPitaPage } from '../pages/chickpea-pita/chickpea-pita';
import { BlackBeanBurritoPage } from '../pages/black-bean-burrito/black-bean-burrito';
import { DinnerPage } from '../pages/dinner/dinner';
import { SpinachPiciPastaPage } from '../pages/spinach-pici-pasta/spinach-pici-pasta';
import { ChineseSteakStewPage } from '../pages/chinese-steak-stew/chinese-steak-stew';
import { HistoryPage } from '../pages/history/history';
import { AllergiesPage } from '../pages/allergies/allergies';
import { ScansPage } from '../pages/scans/scans';
import { MostRecentHeadMRIPage } from '../pages/most-recent-head-mri/most-recent-head-mri';
import { MostRecentHandMRIPage } from '../pages/most-recent-hand-mri/most-recent-hand-mri';
import { MostRecentLungsXrayPage } from '../pages/most-recent-lungs-xray/most-recent-lungs-xray';
import { NewsPage } from '../pages/news/news';
import { Page27Page } from '../pages/page27/page27';
import { MainMenuPage } from '../pages/main-menu/main-menu';
import { ProfilePage } from '../pages/profile/profile';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    CameraTabDefaultPagePage,
    CartTabDefaultPagePage,
    CloudTabDefaultPagePage,
    TabsControllerPage,
    WelcomePage,
    RemindersPage,
    MedicationsPage,
    DoctorsAppointmentsPage,
    HealthyMenusPage,
    BreakfastPage,
    Tab4DefaultPagePage,
    BanofeeStuffedPanCakePage,
    FruityOatCakePage,
    LunchPage,
    ChickpeaPitaPage,
    BlackBeanBurritoPage,
    DinnerPage,
    SpinachPiciPastaPage,
    ChineseSteakStewPage,
    HistoryPage,
    AllergiesPage,
    ScansPage,
    MostRecentHeadMRIPage,
    MostRecentHandMRIPage,
    MostRecentLungsXrayPage,
    NewsPage,
    Page27Page,
    MainMenuPage,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CameraTabDefaultPagePage,
    CartTabDefaultPagePage,
    CloudTabDefaultPagePage,
    TabsControllerPage,
    WelcomePage,
    RemindersPage,
    MedicationsPage,
    DoctorsAppointmentsPage,
    HealthyMenusPage,
    BreakfastPage,
    Tab4DefaultPagePage,
    BanofeeStuffedPanCakePage,
    FruityOatCakePage,
    LunchPage,
    ChickpeaPitaPage,
    BlackBeanBurritoPage,
    DinnerPage,
    SpinachPiciPastaPage,
    ChineseSteakStewPage,
    HistoryPage,
    AllergiesPage,
    ScansPage,
    MostRecentHeadMRIPage,
    MostRecentHandMRIPage,
    MostRecentLungsXrayPage,
    NewsPage,
    Page27Page,
    MainMenuPage,
    ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}